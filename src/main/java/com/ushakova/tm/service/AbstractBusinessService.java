package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IBusinessRepository;
import com.ushakova.tm.api.service.IBusinessService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.exception.system.IncorrectIndexException;
import com.ushakova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    protected IBusinessRepository<E> repository;

    public AbstractBusinessService(IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public E changeStatusById(final String id, final Status status, final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id);
        if (entity == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByIndex(final Integer index, final Status status, final String userId) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final E entity = findByIndex(index, userId);
        if (entity == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByName(final String name, final Status status, final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final E entity = findByName(name, userId);
        if (entity == null) throw new EntityNotFoundException();
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E completeById(final String id, final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id);
        if (entity == null) throw new EntityNotFoundException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public E completeByIndex(final Integer index, final String userId) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final E entity = findByIndex(index, userId);
        if (entity == null) throw new EntityNotFoundException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public E completeByName(final String name, final String userId) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final E entity = findByName(name, userId);
        if (entity == null) throw new EntityNotFoundException();
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator, final String userId) {
        if (comparator == null) return null;
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(comparator, userId);
    }

    @Override
    public E findByIndex(final Integer index, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return repository.findByIndex(index, userId);
    }

    @Override
    public E findByName(final String name, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(name, userId);
    }

    @Override
    public E removeById(final String id, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id, userId);
    }

    @Override
    public E removeByIndex(final Integer index, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return repository.removeByIndex(index, userId);
    }

    @Override
    public E removeByName(String name, String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(name, userId);
    }

    @Override
    public E startById(final String id, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(id);
        if (entity == null) throw new ProjectNotFoundException();
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E startByIndex(final Integer index, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        final E entity = findByIndex(index, userId);
        if (entity == null) throw new TaskNotFoundException();
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E startByName(final String name, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByName(name, userId);
        if (entity == null) throw new TaskNotFoundException();
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    @Override
    public E updateById(final String id, final String name, final String description, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findById(id);
        if (entity == null) throw new ProjectNotFoundException();
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateByIndex(final Integer index, final String name, final String description, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByIndex(index, userId);
        if (entity == null) throw new TaskNotFoundException();
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
