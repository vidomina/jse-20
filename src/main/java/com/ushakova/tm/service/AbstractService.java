package com.ushakova.tm.service;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.api.IService;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

    @Override
    public E removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

}
