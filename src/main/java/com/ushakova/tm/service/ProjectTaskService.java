package com.ushakova.tm.service;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.service.IProjectTaskService;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Task bindTaskByProject(final String projectId, final String taskId, final String userId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findById(taskId, userId);
        if (task == null) return null;
        final Project project = projectRepository.findById(projectId, userId);
        if (project == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    @Override
    public Project removeProjectById(final String projectId, final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId, userId);
        for (final Task task : tasks) {
            taskRepository.removeById(task.getId(), userId);
        }
        return projectRepository.removeById(projectId, userId);
    }

    @Override
    public Task unbindTaskFromProject(final String taskId, final String userId) {
        if (taskId == null || taskId.isEmpty()) return null;
        final Task task = taskRepository.findById(taskId, userId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
