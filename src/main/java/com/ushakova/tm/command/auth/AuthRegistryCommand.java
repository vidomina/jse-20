package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.util.TerminalUtil;

public class AuthRegistryCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "New user registration.";
    }

    @Override
    public void execute() {
        System.out.println("Registration new user:");
        System.out.println("Enter Login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        final String password = TerminalUtil.nextLine();
        System.out.println("Enter Email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

    @Override
    public String name() {
        return "registration";
    }

}
