package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.TerminalUtil;

public class AuthSetPasswordCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set user password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter user id:");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new UserNotFoundException();
        System.out.println("Enter password:");
        final String password = TerminalUtil.nextLine();
        if (serviceLocator.getUserService().setPassword(userId, password) == null) {
            throw new UserNotFoundException();
        }
    }

    @Override
    public String name() {
        return "user-set-password";
    }

}
