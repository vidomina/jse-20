package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;

public class AuthLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        System.out.println("Logout");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String name() {
        return "logout";
    }

}
