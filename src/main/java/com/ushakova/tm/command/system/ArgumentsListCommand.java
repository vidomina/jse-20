package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show application arguments.";
    }

    @Override
    public void execute() {
        final Collection<String> arguments = serviceLocator.getCommandService().getArguments();
        System.out.println("Available arguments:");
        for (final String argument : arguments) {
            System.out.println(argument);
        }
    }

    @Override
    public String name() {
        return "arguments";
    }

}