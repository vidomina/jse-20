package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;
import com.ushakova.tm.util.NumberUtil;

public class SystemInfoCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String description() {
        return "Show system info.";
    }

    @Override
    public void execute() {
        final int processors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("***System Info***");
        System.out.println("***Info***");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public String name() {
        return "info";
    }

}
