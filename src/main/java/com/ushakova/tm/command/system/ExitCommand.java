package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String name() {
        return "exit";
    }

}
