package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;

public class ProjectCompleteProjectByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Set \"Complete\" status to project by id.";
    }

    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().completeById(id, userId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "complete-project-by-id";
    }

}
