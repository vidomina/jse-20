package com.ushakova.tm.command;

import com.ushakova.tm.exception.entity.TaskNotFoundException;
import com.ushakova.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTaskInfo(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId()
                + "\nTitle: " + task.getName()
                + "\nDescription: " + task.getDescription()
                + "\nStatus: " + task.getStatus().getDisplayName()
                + "\nStart Date: " + task.getDateStart()
                + "\nExpiration Date: " + task.getDateFinish()
                + "\nCreate Date: " + task.getDateCreate());
    }

}
