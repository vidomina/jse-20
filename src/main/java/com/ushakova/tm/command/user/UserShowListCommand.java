package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.model.User;

import java.util.List;

public class UserShowListCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "List of users.";
    }

    @Override
    public void execute() {
        System.out.println("User list:");
        List<User> users = serviceLocator.getUserService().findAll();
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Override
    public String name() {
        return "user-list";
    }

}
