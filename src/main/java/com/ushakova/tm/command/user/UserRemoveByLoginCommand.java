package com.ushakova.tm.command.user;

import com.ushakova.tm.command.AbstractUserCommand;
import com.ushakova.tm.exception.entity.UserNotFoundException;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeByLogin(login);
        if (user == null) throw new UserNotFoundException();
    }

    @Override
    public String name() {
        return "user-remove-by-login";
    }

}

