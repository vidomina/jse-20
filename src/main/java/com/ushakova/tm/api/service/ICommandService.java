package com.ushakova.tm.api.service;

import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    Collection<String> getArguments();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandNameList();

    Collection<AbstractCommand> getCommands();

}
