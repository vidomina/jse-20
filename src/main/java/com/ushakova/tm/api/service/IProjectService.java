package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String name, String description, String userId);

}
