package com.ushakova.tm.api.service;

import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // Task Controller
    Task bindTaskByProject(String projectId, String taskId, String userId);

    // Task Controller
    List<Task> findAllTaskByProjectId(String projectId, String userId);

    // Project Controller
    Project removeProjectById(String projectId, String userId);

    // Task Controller
    Task unbindTaskFromProject(String taskId, String userId);

}

