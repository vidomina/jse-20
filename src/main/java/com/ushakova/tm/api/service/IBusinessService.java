package com.ushakova.tm.api.service;

import com.ushakova.tm.api.IService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    E changeStatusById(String id, Status status, String userId);

    E changeStatusByIndex(Integer index, Status status, String userId);

    E changeStatusByName(String name, Status status, String userId);

    E completeById(String id, String userId);

    E completeByIndex(Integer index, String userId);

    E completeByName(String name, String userId);

    List<E> findAll(Comparator<E> comparator, String userId);

    E findByIndex(Integer index, String userId);

    E findByName(String name, String userId);

    E removeById(String id, String userId);

    E removeByIndex(Integer index, String userId);

    E removeByName(String name, String userId);

    E startById(String id, String userId);

    E startByIndex(Integer index, String userId);

    E startByName(String name, String userId);

    E updateById(String id, String name, String description, String userId);

    E updateByIndex(Integer index, String name, String description, String userId);

}
