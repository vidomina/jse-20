package com.ushakova.tm.api.service;

public interface IServiceLocator {

    IAuthService getAuthService();

    ICommandService getCommandService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

}
