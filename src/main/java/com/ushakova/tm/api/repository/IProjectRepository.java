package com.ushakova.tm.api.repository;

import com.ushakova.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
