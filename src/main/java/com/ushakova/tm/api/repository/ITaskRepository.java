package com.ushakova.tm.api.repository;

import com.ushakova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    List<Task> findAllByProjectId(String projectId, String userId);

}