package com.ushakova.tm.api.repository;

import com.ushakova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    Collection<String> getArguments();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandNames();

    Collection<AbstractCommand> getCommands();

}
