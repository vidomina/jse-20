package com.ushakova.tm.api.entity;

import com.ushakova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
