package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IBusinessRepository;
import com.ushakova.tm.exception.empty.EmptyIdException;
import com.ushakova.tm.exception.empty.EmptyNameException;
import com.ushakova.tm.exception.entity.EntityNotFoundException;
import com.ushakova.tm.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @Override
    public E add(final E entity, final String userId) {
        entity.setUserId(userId);
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        final List<E> result = findAll(userId);
        this.entities.removeAll(result);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> list = new ArrayList<>();
        for (final E entity : entities) {
            if (userId.equals(entity.getUserId()))
                list.add(entity);
        }
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator, final String userId) {
        final List<E> list = findAll(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public E findById(final String id, final String userId) {
        if (id == null || id.isEmpty()) return null;
        for (final E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final Integer index, final String userId) {
        final E entity = entities.get(index);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    public E findByName(final String name, final String userId) {
        for (final E entity : entities) {
            if (name.equals(entity.getName()) && userId.equals(entity.getUserId()))
                return entity;
        }
        return null;
    }

    @Override
    public void remove(final E entity, final String userId) {
        if (userId.equals(entity.getUserId())) {
            entities.remove(entity);
        }
    }

    @Override
    public E removeById(final String id, final String userId) {
        final E entity = findById(userId, id);
        if (entity == null) throw new EmptyIdException();
        remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index, final String userId) {
        final E entity = findByIndex(index, userId);
        if (entity == null) throw new EntityNotFoundException();
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByName(final String name, final String userId) {
        final E entity = findByName(name, userId);
        if (name == null) throw new EmptyNameException();
        entities.remove(entity);
        return entity;
    }

}
