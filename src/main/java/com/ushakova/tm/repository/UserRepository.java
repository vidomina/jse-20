package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByEmail(final String email) {
        for (final User user : entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findById(login);
        if (login == null) return null;
        return removeByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        entities.remove(user);
        return user;
    }

}
