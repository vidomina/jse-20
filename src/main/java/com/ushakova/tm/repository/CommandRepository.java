package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> list = new ArrayList<>();

    @Override
    public void add(AbstractCommand command) {
        list.add(command);
    }

    @Override
    public Collection<String> getArguments() {
        final List<String> args = new ArrayList<>();
        for (final AbstractCommand command : list) {
            if (command.arg() != null)
                args.add(command.arg());
        }
        return args;
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        for (final AbstractCommand command : list) {
            if (arg.equals(command.arg())) return command;
        }
        return null;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        for (final AbstractCommand command : list) {
            if (name.equals(command.name())) return command;
        }
        return null;
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> names = new ArrayList<>();
        for (final AbstractCommand command : list) {
            names.add(command.name());
        }
        return names;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return list;
    }

}