package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String projectId, final String userId) {
        final List<Task> tasks = new ArrayList<>();
        for (final Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) tasks.add(task);
        }
        return tasks;
    }

}
